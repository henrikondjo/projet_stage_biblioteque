from Connexion import connection
from datetime import datetime, timedelta
def connexion_user(data):
    conn=connection()
    email=data.get('email')
    password=data.get("password")
    # Vérification si l'utilisateur existe déjà dans la base de données
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM UTILISATEURS WHERE email = '{email}' AND MOT_DE_PASSE = '{password}'")
    result = cur.fetchone()
    if result:
        user = {
            'ID_User': result[0],  # Supposons que la première colonne est l'ID de l'utilisateur
            'Nom': result[1],
            'Prenom': result[2],
            'email': result[3],
            'Pswd':result[4],
            'Adresse':result[5],
            'Telephone':result[6],
            'Sexe':result[7]
                # Ajoutez d'autres champs utilisateur si nécessaire
            }
        cur.close()
        conn.close()
        return user,True
    else:
        return None,False


def get_livres():
    conn = connection()
    cur = conn.cursor()

    try:
        # Requête pour récupérer les informations des livres et le nombre d'exemplaires disponibles
        cur.execute("""
            SELECT Livres.ID_LIVRE, Livres.TITRE, Livres.AUTEUR, Livres.ANNEE, Livres.ID_CATEGORIE, Livres.CHEMIN_LIVRE,
                   CATEGORIES.NOM_CATEGORIE,  -- Ajout de la colonne NOM_CATEGORIE
                   COUNT(Exemplaires.ID_EXEMPLAIRE) AS NOMBRE_EXEMPLAIRES,
                   CASE
                       WHEN COUNT(Exemplaires.ID_EXEMPLAIRE) = 0 THEN 'All copies commanded'
                       ELSE 'You can command now'
                   END AS STATUS
            FROM Livres
            LEFT JOIN Exemplaires ON Livres.ID_LIVRE = Exemplaires.ID_LIVRE
            LEFT JOIN CATEGORIES ON Livres.ID_CATEGORIE = CATEGORIES.ID_CATEGORIE  -- Jointure avec la table CATEGORIES
            GROUP BY Livres.ID_LIVRE, Livres.TITRE, Livres.AUTEUR, Livres.ANNEE, Livres.ID_CATEGORIE, Livres.CHEMIN_LIVRE, CATEGORIES.NOM_CATEGORIE
        """)

        results = cur.fetchall()
        livres_info = [{
            "ID_LIVRE": result[0],
            "TITRE": result[1],
            "AUTEUR": result[2],
            "ANNEE": result[3],
            "ID_CATEGORIE": result[4],
            "CHEMIN_LIVRE": result[5],  # Ajout de la colonne CHEMIN_LIVRE
            "NOM_CATEGORIE": result[6],
            "NOMBRE_EXEMPLAIRES": result[7],
            "STATUS": result[8]
        } for result in results]

        return livres_info

    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()

def inserer_utilisateur(data):
    
    conn = connection()
    cur = conn.cursor()
    try:
        nom = data.get('nom')
        prenom = data.get('prenom')
        email = data.get('email')
        mot_de_passe = data.get('mot_de_passe')
        adresse = data.get('adresse')
        telephone = data.get('telephone')
        sexe = data.get('sexe')

    # Vérification si l'utilisateur existe déjà dans la base de données
        cur = conn.cursor()
        cur.execute(f"SELECT COUNT(*) FROM Utilisateurs WHERE email = '{email}'")
        result = cur.fetchone()
        user_exists = result[0] > 0
        if user_exists:
            return False,None
        # Exécution de la requête d'insertion
        cur.execute("""
            INSERT INTO UTILISATEURS (NOM, PRENOM, EMAIL, MOT_DE_PASSE, ADRESSE, TELEPHONE, SEXE)
            VALUES (%s, %s, %s, %s, %s, %s, %s)
        """, (nom, prenom, email, mot_de_passe, adresse, telephone, sexe))
        cur.execute(f"SELECT * FROM UTILISATEURS WHERE email = '{email}'")
        result = cur.fetchone()
        if result:
            user = {
                'ID_User': result[0],  # Supposons que la première colonne est l'ID de l'utilisateur
                'Nom': result[1],
                'Prenom': result[2],
                'email': result[3],
                'Pswd':result[4],
                'Adresse':result[5],
                'Telephone':result[6],
                'Sexe':result[7]
                    # Ajoutez d'autres champs utilisateur si nécessaire
                }
            # Valider la transaction
        conn.commit()
        cur.close()
        conn.close()
        return True,user

    except Exception as e:
        return False, ({'error': str(e)})


def update_User(data):
    conn=connection()
    nom= data.get('nom')
    prenom = data.get('prenom')
    email = data.get('email')
    password = data.get('password')
    telephone=data.get('telephone')
    adresse=data.get('adresse')
    id_user = data.get("id_user")
    try:
        cur = conn.cursor()
        update_query = """
                  UPDATE UTILISATEURS
                 SET nom = %s, prenom = %s, email = %s, MOT_DE_PASSE = %s
                 , telephone = %s, adresse = %s WHERE id_utilisateur = %s
                """
        cur.execute(update_query, (nom, prenom, email, password,telephone,adresse,id_user))
        conn.commit()
        cur.close()
        return True
    except Exception as e:
         return ({'error': str(e)}), False 


# Étape 1 : Commande du livre
def commander_livre(data):
    conn = connection()
    cur = conn.cursor()
    id_livre=data.get('id_livre')
    id_utilisateur = data.get('id_user')
    try:
        # Vérifier si la commande existe déjà
        cur.execute("SELECT * FROM COMMANDES_EN_ATTENTE WHERE ID_LIVRE = %s AND ID_UTILISATEUR = %s", (id_livre, id_utilisateur))
        result = cur.fetchone()
        if result:
            return False
        # Insérer la commande dans une table temporaire ou spécifique aux commandes en attente
        cur.execute("""
            INSERT INTO COMMANDES_EN_ATTENTE (ID_UTILISATEUR, ID_LIVRE, DATE_COMMANDE,STATUT)
            VALUES (%s, %s, %s, %s)
        """, (id_utilisateur, id_livre, datetime.now().date(),"En attente"))
        conn.commit()
        cur.close()
        conn.close()
        return True
    except Exception as e:
        return {'error': str(e)}, 500
    
def get_Command(data):
    conn = connection()
    cur = conn.cursor()
    id_utilisateur = data.get('id_user')
    
    try:
        # Requête pour récupérer les informations des livres et le nombre d'exemplaires disponibles
        cur.execute("""
            SELECT
                COMMANDES_EN_ATTENTE.ID_COMMANDE,
                LIVRES.TITRE AS NOM_LIVRE,
                LIVRES.AUTEUR,
                LIVRES.ANNEE,
                LIVRES.CHEMIN_LIVRE,  -- Ajout du chemin du livre
                CATEGORIES.NOM_CATEGORIE,
                COMMANDES_EN_ATTENTE.DATE_COMMANDE,
                COMMANDES_EN_ATTENTE.STATUT
            FROM
                COMMANDES_EN_ATTENTE
            JOIN
                LIVRES ON COMMANDES_EN_ATTENTE.ID_LIVRE = LIVRES.ID_LIVRE
            JOIN
                CATEGORIES ON LIVRES.ID_CATEGORIE = CATEGORIES.ID_CATEGORIE
            WHERE
                COMMANDES_EN_ATTENTE.ID_UTILISATEUR = %s
            ORDER BY
                COMMANDES_EN_ATTENTE.DATE_COMMANDE DESC
        """, (id_utilisateur,))

        results = cur.fetchall()
        livres_info = [{
            "ID_COMMAND": result[0],
            "TITRE": result[1],
            "AUTEUR": result[2],
            "ANNEE": result[3],
            "CHEMIN_LIVRE": result[4],  # Chemin du livre
            "NOM_CATEGORIE": result[5],  # Ajout de la colonne NOM_CATEGORIE
            "DATE": result[6],
            "STATUS": result[7]
        } for result in results]
        
        cur.close()
        conn.close()
        return True, livres_info
    except Exception as e:
        return {'error': str(e)}, 500

def delete_Command(data):
    conn=connection()
    cur = conn.cursor()
    id_command = data.get('id_command')
    cur.execute("DELETE FROM COMMANDES_EN_ATTENTE WHERE ID_COMMANDE = %s", (id_command,))
    conn.commit()
    cur.close()
    return True

# Ajoutez cette fonction dans votre fichier ControllersUsers.py

def get_Command_count(data):
    conn = connection()
    cur = conn.cursor()
    id_utilisateur = data.get('id_user')

    try:
        # Requête pour compter le nombre de commandes de l'utilisateur
        cur.execute("""
            SELECT COUNT(*) FROM COMMANDES_EN_ATTENTE
            WHERE ID_UTILISATEUR = %s;
        """, (id_utilisateur,))

        count = cur.fetchone()[0]
        cur.close()
        conn.close()
        return count
    except Exception as e:
        return {'error': str(e)}, 500



def get_emprunt_user(data):
    conn = connection()
    cur = conn.cursor()
    id_utilisateur = data.get('id_user')
    try:
        cur.execute("""
            SELECT
            LIVRES.TITRE,
            EMPRUNTS.DATE_EMPRUNT,
            EMPRUNTS.DATE_RETOUR,
            EMPRUNTS.DATE_ESTIMATION_RET,
            EMPRUNTS.STATUT,
            EMPRUNTS.ID_EXEMPLAIRE
            FROM
                BIBLIOTECH.PUBLIC.EMPRUNTS
            JOIN
                BIBLIOTECH.PUBLIC.UTILISATEURS ON EMPRUNTS.ID_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
                BIBLIOTECH.PUBLIC.EXEMPLAIRES ON EMPRUNTS.ID_EXEMPLAIRE = EXEMPLAIRES.ID_EXEMPLAIRE
            JOIN
                BIBLIOTECH.PUBLIC.LIVRES ON EXEMPLAIRES.ID_LIVRE = LIVRES.ID_LIVRE
            WHERE
            EMPRUNTS.ID_UTILISATEUR = %s;
            """, (id_utilisateur,))

        results = cur.fetchall()
        emprunt_info = [{
            "TITRE": result[0],
            "DATE_EMPRUNT": result[1],
            "DATE_RETOUR": result[2],
            "DATE_ESTIMATION": result[3],
            "STATUT": result[4],
            "ID_EXEMPLAIRE": result[5]
        } for result in results]

        return emprunt_info
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()

def inserer_message_utilisateur(data):
    conn = connection()
    cur = conn.cursor()

    try:
        id_emetteur_utilisateur = data.get('id_emetteur_utilisateur')
        id_destinataire_admin = data.get('id_destinataire_admin')
        contenu_message = data.get('contenu_message')
        titre_message = data.get('titre_message')
        date_envoi = datetime.now().date()

        # Exécution de la requête d'insertion
        cur.execute("""
            INSERT INTO MESSAGES (
                ID_EMETTEUR_UTILISATEUR, 
                ID_DESTINATAIRE_ADMIN, 
                CONTENU_MESSAGE, 
                TITRE_MESSAGE, 
                DATE_ENVOI
            )
            VALUES (%s, %s, %s, %s, %s)
        """, (
            id_emetteur_utilisateur,
            id_destinataire_admin,
            contenu_message,
            titre_message,
            date_envoi
        ))

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return True
    except Exception as e:
        return False, {'error': str(e)}



def lire_messages_user(data):
    conn = connection()
    cur = conn.cursor()
    id_user=data.get('id_user')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                admin.USERNAME
            FROM
                MESSAGES
    
                 JOIN
                ADMIN ON MESSAGES.ID_EMETTEUR_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_DESTINATAIRE_UTILISATEUR = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_user,))

        results = cur.fetchall()
        messages_info = [{
            "ID_MESSAGE": result[0],
            "CONTENU_MESSAGE": result[1],
            "TITRE_MESSAGE": result[2],
            "DATE_ENVOI": result[3],
            "STATUT_LU": result[4],
            "USERNAME": result[5]
        } for result in results]

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info
    except Exception as e:
        return False, {'error': str(e)}
    


def lire_one_messages_user(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
                      SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                ADMIN.USERNAME AS USERNAME
            FROM
                MESSAGES
                
         JOIN
                UTILISATEURS ON MESSAGES.ID_DESTINATAIRE_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
               ADMIN ON MESSAGES.ID_EMETTEUR_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_MESSAGE = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_message,))

        result = cur.fetchone()
        messages_info = {
             "ID_MESSAGE": result[0],
            "NOM_EMETTEUR": result[1],
            "PRENOM_EMETTEUR": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6],
            "USERNAME":result[7]
        } 

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}


def marquer_message_comme_lu(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')
    try:
        # Exécution de la requête de mise à jour
        cur.execute("""
            UPDATE MESSAGES
            SET STATUT_LU = TRUE
            WHERE ID_MESSAGE = %s
        """, (id_message,))

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return True
    except Exception as e:
        return False, {'error': str(e)}


def lire_messages_envoyer_user(data):
    conn = connection()
    cur = conn.cursor()
    id_user=data.get('id_user')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                ADMIN.USERNAME    
            FROM
                MESSAGES
            JOIN
                ADMIN ON MESSAGES.ID_DESTINATAIRE_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_EMETTEUR_UTILISATEUR = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_user,))

        results = cur.fetchall()
        messages_info = [{
            "ID_MESSAGE": result[0],
            "CONTENU_MESSAGE": result[1],
            "TITRE_MESSAGE": result[2],
            "DATE_ENVOI": result[3],
            "STATUT_LU": result[4],
            "USERNAME":result[5]
        } for result in results]

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}



def lire_one_messages_envoye_user(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
             SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                ADMIN.USERNAME AS USERNAME
            FROM
                MESSAGES
            JOIN
                UTILISATEURS ON MESSAGES.ID_EMETTEUR_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
                ADMIN ON MESSAGES.ID_DESTINATAIRE_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_MESSAGE = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_message,))

        result = cur.fetchone()
        messages_info = {
            "ID_MESSAGE": result[0],
            "NOM_EMETTEUR": result[1],
            "PRENOM_EMETTEUR": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6],
            "USERNAME":result[7]
        } 

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}

# def Delete_Message(data):
#     conn = connection()
#     cur = conn.cursor()
#     id_message=data.get('id_message')

#     try:
#         # Exécution de la requête pour récupérer les messages destinés à l'administrateur
#         cur.execute("""
#             DELETE FROM MESSAGES
#             WHERE
#                 MESSAGES.ID_MESSAGE = %s
            
#         """, (id_message,))
#         # Valider la transaction
#         conn.commit()
#         # Fermer la connexion
#         cur.close()
#         conn.close()
#         return True
#     except Exception as e:
#         return False, {'error': str(e)}

