from flask import Flask, jsonify, request
from ControllersUsers import *
from ControllersAdmin import *
from chatbot import *
from flask_cors import CORS  # Importez CORS depuis flask_cors
app = Flask(__name__)
CORS(app)  # Activez CORS pour votre application Flask



@app.route('/api/loginAdmin', methods=['POST'])
def LoginAdmin():
    data = request.get_json()
    print(data)
    user,res=connexion_admin(data)
    if res:
        response = {'success':True,'data':user}
    else:
        response = {'success':False,'data':None}
    return jsonify(response)

@app.route('/api/modifAdmin', methods=['PUT'])
def Modif():
    data = request.get_json()
    print(data)
    res=update_Admin(data)
    if res:
        response = {'success':True,'msg':'Informations mises à jour avec succès'}
    else:
        response = {'success':False,'msg':'Impossible de faire la mise à jour avec succès'}

    return jsonify(response)

@app.route('/api/ajouterCategorie', methods=['POST'])
def Ajouter_Cate():
    data = request.get_json()
    print(data)
    res=Ajouter_categories(data)
    if res:
        response = {'success':True}
    else:
        response = {'success':False}
    return jsonify(response)

@app.route('/api/getallcat', methods=['GET'])
def getall_Cate():
    res=findcat()
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)


@app.route('/api/ajouterLivre', methods=['POST'])
def Ajout_Livre():
    data = request.get_json()
    # print(data)
    res,id=Ajouter_Livre(data)
    print(id)
    if res:
        response = {'success':True}
    else:
        response = {'success':False}
    return jsonify(response)

@app.route('/api/getallusers', methods=['GET'])
def getall_Users():
    res=get_users()
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)
@app.route('/api/getallLivres', methods=['GET'])
def getall_Livres():
    res=get_livres_info()
    print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/modif_Livre', methods=['PUT'])
def Modif_Livres():
    data = request.get_json()
    print(data)
    res=update_Livres(data)
    if res:
        response = {'success':True,'msg':'Informations mises à jour avec succès'}
    else:
        response = {'success':False,'msg':'Impossible de faire la mise à jour avec succès'}

    return jsonify(response)

@app.route('/api/delete_livre', methods=['DELETE'])
def Delete_Livres():
    data = request.get_json()
    print(data)
    res=delete_Livres(data)
    if res:
        response = {'success':True,'msg':'Livre supprimé avec success'}
    else:
        response = {'success':False,'msg':'non autorisé'}

    return jsonify(response)

@app.route('/api/get_un_livre', methods=['POST'])
def getLivre():
    data = request.get_json()
    # print(data)
    res=get_un_livre(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/getall_Command_count', methods=['GET'])
def get_nb_Command():
    res=getall_Command_count()
    response = {'data':res}
    return jsonify(response)

@app.route('/api/Admin/getall_book_count', methods=['GET'])
def get_nb_Book():
    res=getall_Book_count()
    response = {'data':res}
    return jsonify(response)

@app.route('/api/Admin/getall_user_count', methods=['GET'])
def get_nb_Users():
    res=getall_User_count()
    response = {'data':res}
    return jsonify(response)

@app.route('/api/Admin/getall_emprunt_count', methods=['GET'])
def get_nb_emprunts():
    res=getall_Emprunt_count()
    response = {'data':res}
    return jsonify(response)




@app.route('/api/Admin/get_all_command_users', methods=['GET'])
def getall_command_users():
    res=get_all_command()
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/get_une_command/<int:id_command>', methods=['GET'])
def get_une_command_user(id_command):
    data = {'id_command': id_command}
    print(data)
    res=get_une_command(data)
    print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)



@app.route('/api/Admin/emprunter_livre', methods=['POST'])
def send_emprunt_livre():
    data = request.get_json()
    # print(data)
    res=emprunter_livre(data)
    # print(res)
    if res:
        result = {'success':True,'msg':'emprunt valider avec success'}
    else:
        result = {'success':False,'msg':'invalid emprunt'}
    return jsonify(result)


@app.route('/api/Admin/get_all_emprunt_users', methods=['GET'])
def getall_emprunt_users():
    res=get_all_emprunt()
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/get_info_exemplaire/<int:id_exemplaire>', methods=['GET'])
def getinfo_exemplaire(id_exemplaire):
    data = {'id_exemplaire': id_exemplaire}
    print(data)
    res=get_info_exemplaire(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/retour/livre', methods=['POST'])
def back_books():
    data = request.get_json()
    # print(data)
    res=retourner_livre(data)
    if res:
        response = {'success':True,'msg':'Livre retouné avec success'}
    else:
        response = {'success':False,'msg':'Non autorisé'}
    return jsonify(response)


@app.route('/api/Admin/envoyer/message',methods=['POST'] )
def send_message_Admin():
    data = request.get_json()
    res=inserer_message_admin(data)
    if res:
        response={'success': True, 'msg':"Message envoyé"}
    else:
        response={'success': False, 'msg':"Message non  envoyé"}
    return jsonify(response)

@app.route('/api/Admin/get_message/<int:id_admin>', methods=['GET'])
def get_message_Admin(id_admin):
    data = {'id_admin': id_admin}
    print(data)
    res=lire_messages_admin(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)


@app.route('/api/Admin/get_message_envoye/<int:id_admin>', methods=['GET'])
def get_message_envoye_Admin(id_admin):
    data = {'id_admin': id_admin}
    print(data)
    res=lire_messages_envoyer_admin(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/get_One_message/<int:id_message>', methods=['GET'])
def get_ONE_message_Admin(id_message):
    data = {'id_message': id_message}
    print(data)
    res=lire_one_messages_admin(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/Admin/get_One_message_envoyer/<int:id_message>', methods=['GET'])
def get_ONE_message_envoyer_Admin(id_message):
    data = {'id_message': id_message}
    print(data)
    res=lire_one_messages_envoye_admin(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/user/delete_message/<int:id_message>', methods=['DELETE'])
def Del_Message(id_message):
    data = {'id_message': id_message}
    print(data)
    res=Delete_Message(data)
    if res:
        response = {'success':True,'msg':'Message supprimé avec success'}
    else:
        response = {'success':False,'msg':'non autorisé'}
    return jsonify(response)





# #################################
@app.route('/api/loginUser', methods=['POST'])
def LoginUser():
    data = request.get_json()
    # print(data)
    user,res=connexion_user(data)
    if res:
        response = {'success':True,'data':user}
    else:
        response = {'success':False,'data':None}
    return jsonify(response)


@app.route('/api/register/user', methods=['POST'])
def register():
    data = request.get_json()
    # print(data)
    res,user=inserer_utilisateur(data)
    if res:
        response = {'success':True,'data':user}
        # print(user)
    else:
        response = {'success':False,'data':None}
    return jsonify(response)



@app.route('/api/modifUser', methods=['PUT'])
def Modif_User():
    data = request.get_json()
    # print(data)
    res=update_User(data)
    if res:
        response = {'success':True,'msg':'Informations mises à jour avec succès'}
    else:
        response = {'success':False,'msg':'cet adresse courriel existe déjà'}

    return jsonify(response)


@app.route('/api/get_livres_user', methods=['GET'])
def getall_Livres_user():
    res=get_livres()
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)


@app.route('/api/user/commander', methods=['POST'])
def User_Commande():
    data = request.get_json()
    print(data)
    res=commander_livre(data)
    if res:
        response = {'success':True}
    else:
        response = {'success':False}
    return jsonify(response)


@app.route('/api/user/get_Command', methods=['POST'])
def get_User_Command():
    data = request.get_json()
    print(data)
    val,res=get_Command(data)
    if val:
        response = {'success':True,'data':res}
    else:
        response = {'success':False,'data':'NOT FOUND'}
    return jsonify(response)


@app.route('/api/user/delete_command/<int:id_command>', methods=['DELETE'])
def Del_Command(id_command):
    data = {'id_command': id_command}
    print(data)
    res=delete_Command(data)
    if res:
        response = {'success':True,'msg':'Command supprimé avec success'}
    else:
        response = {'success':False,'msg':'non autorisé'}
    return jsonify(response)



@app.route('/api/user/get_Command_count', methods=['POST'])
def get_nb_User_Command():
    data = request.get_json()
    print(data)
    res=get_Command_count(data)
    response = {'data':res}
    return jsonify(response)



@app.route('/api/user/emprunt/<int:id_user>', methods=['GET'])
def get_emprunts(id_user):
    data = {'id_user': id_user}
    print(data)
    res=get_emprunt_user(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)


@app.route('/api/user/envoyer/message',methods=['POST'] )
def send_message_User():
    data = request.get_json()
    res=inserer_message_utilisateur(data)
    if res:
        response={'success': True, 'msg':"Message envoyé"}
    else:
        response={'success': False, 'msg':"Message non  envoyé"}
    return jsonify(response)

@app.route('/api/user/get_message/<int:id_user>', methods=['GET'])
def get_message_User(id_user):
    data = {'id_user': id_user}
    print(data)
    res=lire_messages_user(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/user/get_One_message/<int:id_message>', methods=['GET'])
def get_ONE_message_User(id_message):
    data = {'id_message': id_message}
    print(data)
    res=lire_one_messages_user(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)


@app.route('/api/user/get_message_envoye/<int:id_user>', methods=['GET'])
def get_message_envoye_user(id_user):
    data = {'id_user': id_user}
    print(data)
    res=lire_messages_envoyer_user(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)

@app.route('/api/user/get_One_message_envoyer/<int:id_message>', methods=['GET'])
def get_ONE_message_envoyer_user(id_message):
    data = {'id_message': id_message}
    print(data)
    res=lire_one_messages_envoye_user(data)
    # print(res)
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)




@app.route('/api/getallAdmins', methods=['GET'])
def getall_Admins():
    res=get_admins()
    result = {'success':True,'data':res}
    # print(result)
    return jsonify(result)




@app.route('/api/chatbot', methods=['POST'])
def chatbot():
    data = request.get_json()
    user_input = data['user_input']
    output = generate(user_input)
    return jsonify({'response': output})










if __name__ == "__main__":
    app.run(debug=True)

