from Connexion import connection
from faker import Faker
import random


conn = connection()

# Création du curseur
cur = conn.cursor()

# Utilisation de Faker pour générer des données aléatoires
fake = Faker()

# Insertion de 100 utilisateurs fictifs
for _ in range(100):
    nom = fake.last_name()
    prenom = fake.first_name()
    email = fake.email()
    mot_de_passe = fake.password()
    adresse = fake.address()
    telephone = fake.phone_number()
    sexe = random.choice(['Homme', 'Femme'])

    # Exécution de la requête d'insertion
    cur.execute("""
        INSERT INTO UTILISATEURS (NOM, PRENOM, EMAIL, MOT_DE_PASSE, ADRESSE, TELEPHONE, SEXE)
        VALUES (%s, %s, %s, %s, %s, %s, %s)
    """, (nom, prenom, email, mot_de_passe, adresse, telephone, sexe))

# Validation de la transaction
conn.commit()

# Fermeture du curseur et de la connexion
cur.close()
conn.close()
