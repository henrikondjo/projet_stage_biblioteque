from Connexion import connection
from datetime import datetime, timedelta
#connectiode l'admin
def connexion_admin(data):
    conn=connection()
    matricule=data.get('matricule')
    password=data.get("password")
    # Vérification si l'utilisateur existe déjà dans la base de données
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM Admin WHERE matricule = '{matricule}' AND password = '{password}'")
    result = cur.fetchone()
    if result:
        user = {
            'id_Admin': result[0],  # Supposons que la première colonne est l'ID de l'utilisateur
            'username': result[1],
            'email': result[3],
            'matricule': result[4],
            'telephone':result[6],
            'password':result[2]
                # Ajoutez d'autres champs utilisateur si nécessaire
            }
        cur.close()
        conn.close()
        return user,True
    else:
        return None,False

def update_Admin(data):
    conn=connection()
    username= data.get('username')
    matricule = data.get('matricule')
    email = data.get('email')
    password = data.get('password')
    telephone=data.get('telephone')
    id_Admin = data.get("id_Admin")
    try:
        cur = conn.cursor()
        update_query = """
                  UPDATE Admin
                 SET username = %s, matricule = %s, email = %s, password = %s
                 , telephone = %s WHERE id_Admin = %s
                """
        cur.execute(update_query, (username, matricule, email, password,telephone,id_Admin))
        conn.commit()
        cur.close()
        return True
    except Exception as e:
         return ({'error': str(e)}), 500

def Ajouter_categories(data):
    conn=connection()
    cur=conn.cursor()
    nom = data.get('Nom_Categorie')
    # Vérification si l'utilisateur existe déjà dans la base de données
    cur = conn.cursor()
    cur.execute(f"SELECT COUNT(*) FROM Categories WHERE Nom_Categorie = '{nom}'")
    result = cur.fetchone()
    cate_exists = result[0] > 0

    if cate_exists:
        return False
    else:
        # Insertion des données dans la table patient
        cur.execute(f"INSERT INTO Categories (Nom_Categorie) VALUES ('{nom}')")

        conn.commit()  # Valider la transaction
        cur.close()
        conn.close()
        return True
    
def findcat():
    conn=connection()
    cur=conn.cursor()
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM Categories")
    results = cur.fetchall()
    categories = [{"ID": result[0], "Name": result[1]} for result in results]
    return categories



def get_id_livre_by_titre(titre):
    conn = connection()
    cur = conn.cursor()

    try:
        # Sélectionner l'ID du livre en fonction du titre
        cur.execute("SELECT ID_LIVRE FROM Livres WHERE TITRE = %s", (titre,))
        id_livre = cur.fetchone()

        if id_livre:
            return id_livre[0]
        else:
            return None
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()

# ControllersAdmin.py

def Ajouter_Livre(data):
    conn = connection()
    cur = conn.cursor()
    titre = data.get('titre')
    auteur = data.get('auteur')
    annee = data.get('annee')
    id_categorie = data.get('id_categorie')
    nb_exemplaires = data.get('nb_exemplaires')  # Nombre d'exemplaires du livre

    try:
        # cur.execute(f"SELECT ID_LIVRE FROM LIVRES")
        # results = cur.fetchall()
        # id_livre= results[-1]
        # # Insérer le livre dans la table LIVRES
        cur.execute("""
            INSERT INTO Livres (TITRE, AUTEUR, ANNEE, ID_CATEGORIE)
            VALUES (%s, %s, %s, %s)
        """, (titre, auteur, annee, id_categorie))
        ID=get_id_livre_by_titre(titre)
        # Insérer le nombre d'exemplaires dans la table EXEMPLAIRES
        for _ in range(nb_exemplaires):
            cur.execute("""
                INSERT INTO EXEMPLAIRES (ID_LIVRE, DISPONIBILITE)
                VALUES (%s, %s)
            """, (ID, 'Disponible'))

        # Valider la transaction
        conn.commit()
        cur.close()
        conn.close()
        return True,ID
    except Exception as e:

        return {'error': str(e)}, False
   
def get_users():
    conn=connection()
    cur=conn.cursor()
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM Utilisateurs")
    results = cur.fetchall()
    users = [{"ID_UTILISATEUR": result[0], "NOM": result[1], "PRENOM": result[2], "EMAIL": result[3], "MOT_DE_PASSE": result[4], "ADRESSE": result[5], "TELEPHONE": result[6], "SEXE": result[7]} for result in results]
    return users

# def get_livres_info():
#     conn = connection()
#     cur = conn.cursor()

#     try:
#         # Requête pour récupérer les informations des livres et le nombre d'exemplaires disponibles
#         cur.execute("""
#             SELECT Livres.ID_LIVRE, Livres.TITRE, Livres.AUTEUR, Livres.ANNEE, Livres.ID_CATEGORIE,
#                    COUNT(Exemplaires.ID_EXEMPLAIRE) AS NOMBRE_EXEMPLAIRES,
#                    CASE
#                        WHEN COUNT(Exemplaires.ID_EXEMPLAIRE) = 0 THEN 'All copies commanded'
#                        ELSE 'You can command now'
#                    END AS STATUS
#             FROM Livres
#             LEFT JOIN Exemplaires ON Livres.ID_LIVRE = Exemplaires.ID_LIVRE
#             GROUP BY Livres.ID_LIVRE, Livres.TITRE, Livres.AUTEUR, Livres.ANNEE, Livres.ID_CATEGORIE
#         """)

#         results = cur.fetchall()
#         livres_info = [{
#             "ID_LIVRE": result[0],
#             "TITRE": result[1],
#             "AUTEUR": result[2],
#             "ANNEE": result[3],
#             "ID_CATEGORIE": result[4],
#             "NOMBRE_EXEMPLAIRES": result[5],
#             "STATUS": result[6]
#         } for result in results]

#         return livres_info

#     except Exception as e:
#         return {'error': str(e)}, 500
#     finally:
#         cur.close()
#         conn.close()

def get_livres_info():
    conn = connection()
    cur = conn.cursor()

    try:
        # Requête pour récupérer les informations des livres, le nombre d'exemplaires et le nom de la catégorie
        cur.execute("""
           SELECT
            Livres.ID_LIVRE,
            Livres.TITRE,
            Livres.AUTEUR,
            Livres.ANNEE,
            Livres.CHEMIN_LIVRE,
            Livres.ID_CATEGORIE,
            CATEGORIES.NOM_CATEGORIE,
            COUNT(Exemplaires.ID_EXEMPLAIRE) AS NOMBRE_EXEMPLAIRES
            FROM Livres
            LEFT JOIN Exemplaires ON Livres.ID_LIVRE = Exemplaires.ID_LIVRE
            LEFT JOIN CATEGORIES ON Livres.ID_CATEGORIE = CATEGORIES.ID_CATEGORIE
            GROUP BY
            Livres.ID_LIVRE,
            Livres.TITRE,
            Livres.AUTEUR,
            Livres.ANNEE,
            Livres.CHEMIN_LIVRE, -- Ajout de cette ligne à la clause GROUP BY
            Livres.ID_CATEGORIE,
            CATEGORIES.NOM_CATEGORIE;

        """)

        results = cur.fetchall()
        livres_info = [{
            "ID_LIVRE": result[0],
            "TITRE": result[1],
            "AUTEUR": result[2],
            "ANNEE": result[3],
            "ID_CATEGORIE": result[5],
            "NOM_CATEGORIE": result[6],
            "CHEMIN_LIVRE": result[4],
            "NOMBRE_EXEMPLAIRES": result[7]
        } for result in results]

        return livres_info

    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()


def update_Livres(data):
    conn=connection()
    id_livre= data.get('id_livre')
    titre= data.get('titre')
    auteur = data.get('auteur')
    annee = data.get('annee')
    id_categorie = data.get('id_categorie')
    try:
        cur = conn.cursor()
        update_query = """
                  UPDATE LIVRES
                 SET TITRE = %s, AUTEUR = %s, ANNEE = %s,ID_CATEGORIE = %s
                  WHERE ID_LIVRE = %s
                """
        cur.execute(update_query, (titre, auteur, annee, id_categorie,id_livre))
        conn.commit()
        cur.close()
        return True
    except Exception as e:
         return ({'error': str(e)}), 500
    
def delete_Livres(data):
    conn=connection()
    cur = conn.cursor()
    id_livre = data.get('id_livre')
    matricule = data.get('matricule')
    cur.execute("SELECT Matricule FROM Admin WHERE Matricule = %s", (matricule,))
    matri_admin = cur.fetchone()
    if matri_admin:
        cur.execute(f"DELETE FROM LIVRES WHERE ID_LIVRE={id_livre}")
        cur.execute(f"DELETE FROM EXEMPLAIRES WHERE ID_LIVRE={id_livre}")
        conn.commit()
        cur.close()
        return True
    else :
        return False

# Étape 2 : Emprunt du livre
def emprunter_livre(data):
    conn = connection()
    cur = conn.cursor()
    id_livre=data.get('id_livre')
    id_utilisateur = data.get("id_user")
    try:
        # Vérifier si le livre a été commandé
        cur.execute("""
            SELECT ID_COMMANDE, DATE_COMMANDE
            FROM COMMANDES_EN_ATTENTE
            WHERE ID_UTILISATEUR = %s AND ID_LIVRE = %s
        """, (id_utilisateur, id_livre))

        result = cur.fetchone()

        if result:
            id_commande= result[0]

            # Supprimer la commande
            cur.execute("""
                DELETE FROM COMMANDES_EN_ATTENTE
                WHERE ID_COMMANDE = %s
            """, (id_commande,))

            # Récupérer un exemplaire disponible du livre
            cur.execute("""
                SELECT ID_EXEMPLAIRE
                FROM EXEMPLAIRES
                WHERE ID_LIVRE = %s AND DISPONIBILITE = 'Disponible'
                LIMIT 1
            """, (id_livre,))

            id_exemplaire = cur.fetchone()

            if id_exemplaire:
                id_exemplaire = id_exemplaire[0]

                # Mettre à jour la disponibilité de l'exemplaire
                cur.execute("""
                    UPDATE EXEMPLAIRES
                    SET DISPONIBILITE = 'Emprunté'
                    WHERE ID_EXEMPLAIRE = %s
                """, (id_exemplaire,))

                # Insérer l'emprunt dans la table EMPRUNTS
                date_emprunt = datetime.now().date()
                date_estimation_retour = date_emprunt + timedelta(days=14)  # Exemple, ajustez selon vos besoins

                cur.execute("""
                INSERT INTO EMPRUNTS (ID_UTILISATEUR, ID_EXEMPLAIRE, DATE_EMPRUNT, DATE_RETOUR, DATE_ESTIMATION_RET, STATUT)
                VALUES (%s, %s, %s, %s, %s, %s)
            """, (id_utilisateur, id_exemplaire, date_emprunt, None, date_estimation_retour, 'En cours'))

                conn.commit()
                return True
            else:
                return False
        else:
            return False

    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()


# Étape 3 : Retour du livre
def retourner_livre(data):
    conn = connection()
    cur = conn.cursor()
    id_exemplaire=data.get('id_exemplaire')
    id_utilisateur = data.get("id_user")
    try:
        # Vérifier si l'utilisateur a effectivement emprunté le livre
        cur.execute("""
            SELECT ID_EMPRUNT, DATE_RETOUR
            FROM EMPRUNTS
            WHERE ID_UTILISATEUR = %s AND ID_EXEMPLAIRE = %s AND STATUT = 'En cours'
        """, (id_utilisateur, id_exemplaire))

        result = cur.fetchone()

        if result:
            id_emprunt= result[0]

            # Mettre à jour la date de retour
            date_retour_effectif = datetime.now().date()

            cur.execute("""
                UPDATE EMPRUNTS
                SET DATE_RETOUR = %s, STATUT = 'Retourné'
                WHERE ID_EMPRUNT = %s
            """, (date_retour_effectif, id_emprunt))

            # Mettre à jour la disponibilité de l'exemplaire
            cur.execute("""
                UPDATE EXEMPLAIRES
                SET DISPONIBILITE = 'Disponible'
                WHERE ID_EXEMPLAIRE = %s
            """, (id_exemplaire,))

            conn.commit()
            return True
        else:
            return False

    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()


def get_un_livre(data):
    conn = connection()
    cur = conn.cursor()
    id_Livre = data.get('id_livre')

    # Utilisez une LEFT JOIN pour récupérer également le nom de la catégorie
    cur.execute("""
        SELECT LIVRES.*, CATEGORIES.NOM_CATEGORIE, LIVRES.CHEMIN_LIVRE
        FROM BIBLIOTECH.PUBLIC.LIVRES
        LEFT JOIN BIBLIOTECH.PUBLIC.CATEGORIES ON LIVRES.ID_CATEGORIE = CATEGORIES.ID_CATEGORIE
        WHERE LIVRES.ID_LIVRE = %s
    """, (id_Livre,))

    result = cur.fetchone()

    livre = {
        'id_livre': result[0],
        'titre': result[1],
        'auteur': result[2],
        'annee': result[3],
        'id_categorie': result[4],
        'nom_categorie': result[6],
        'chemin_livre': result[7]  # Ajout du chemin de l'image
        # Ajoutez d'autres champs utilisateur si nécessaire
    }

    cur.close()
    conn.close()
    return livre


# Ajoutez cette fonction dans votre fichier ControllersUsers.py

def getall_Command_count():
    conn = connection()
    cur = conn.cursor()
    try:
        # Requête pour compter le nombre de commandes de l'utilisateur
        cur.execute("""
            SELECT COUNT(*) FROM COMMANDES_EN_ATTENTE;
        """)
        count = cur.fetchone()[0]
        cur.close()
        conn.close()
        return count
    except Exception as e:
        return {'error': str(e)}, 500

def getall_Book_count():
    conn = connection()
    cur = conn.cursor()
    try:
        # Requête pour compter le nombre de commandes de l'utilisateur
        cur.execute("""
            SELECT COUNT(*) FROM EXEMPLAIRES;
        """)
        count = cur.fetchone()[0]
        cur.close()
        conn.close()
        return count
    except Exception as e:
        return {'error': str(e)}, 500

def getall_User_count():
    conn = connection()
    cur = conn.cursor()
    try:
        # Requête pour compter le nombre de commandes de l'utilisateur
        cur.execute("""
            SELECT COUNT(*) FROM UTILISATEURS;
        """)
        count = cur.fetchone()[0]
        cur.close()
        conn.close()
        return count
    except Exception as e:
        return {'error': str(e)}, 500
    

def get_all_command():
    conn = connection()
    cur = conn.cursor()
    try:
        cur.execute("""
            SELECT
                LIVRES.TITRE,
                LIVRES.CHEMIN_LIVRE,  -- Ajout du chemin du livre
                UTILISATEURS.SEXE AS SEXE_UTILISATEUR,
                UTILISATEURS.NOM AS NOM_UTILISATEUR,
                UTILISATEURS.PRENOM,
                DATE(COMMANDES_EN_ATTENTE.DATE_COMMANDE),
                COMMANDES_EN_ATTENTE.ID_COMMANDE,
                COMMANDES_EN_ATTENTE.ID_UTILISATEUR,
                COMMANDES_EN_ATTENTE.ID_LIVRE
            FROM
                BIBLIOTECH.PUBLIC.COMMANDES_EN_ATTENTE
            JOIN
                BIBLIOTECH.PUBLIC.LIVRES ON COMMANDES_EN_ATTENTE.ID_LIVRE = LIVRES.ID_LIVRE
            JOIN
                BIBLIOTECH.PUBLIC.UTILISATEURS ON COMMANDES_EN_ATTENTE.ID_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR;
        """)

        results = cur.fetchall()
        commande_info = [{
            "TITRE": result[0],
            "CHEMIN_LIVRE": result[1],  # Ajout du chemin du livre
            "SEXE": result[2],
            "NOM": result[3],
            "PRENOM": result[4],
            "DATE": result[5],
            "ID_COMMANDE": result[6],
            "ID_USER": result[7],
            "ID_LIVRE": result[8]
        } for result in results]

        return commande_info
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()



def get_une_command(data):
    conn = connection()
    cur = conn.cursor()
    id_Command = data.get('id_command')
    try:
        cur.execute("""
            SELECT
            LIVRES.ID_LIVRE,
            LIVRES.TITRE,
            UTILISATEURS.ID_UTILISATEUR,
            UTILISATEURS.NOM AS NOM_UTILISATEUR,
            UTILISATEURS.PRENOM
            FROM
                BIBLIOTECH.PUBLIC.COMMANDES_EN_ATTENTE
            JOIN
                BIBLIOTECH.PUBLIC.LIVRES ON COMMANDES_EN_ATTENTE.ID_LIVRE = LIVRES.ID_LIVRE
            JOIN
                BIBLIOTECH.PUBLIC.UTILISATEURS ON COMMANDES_EN_ATTENTE.ID_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            WHERE
            COMMANDES_EN_ATTENTE.ID_COMMANDE =%s;

            """,(id_Command,))

        result= cur.fetchone()
        if result:  # Vérifier si la requête a renvoyé des résultats
            commande_info = {
                "ID_LIVRE": result[0],
                "TITRE": result[1],
                "ID_USER": result[2],
                "NOM": result[3],
                "PRENOM": result[4]
            }
            return commande_info
        else:
            return {'error': 'Aucun résultat trouvé'}, 404
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()


def get_all_emprunt():
    conn = connection()
    cur = conn.cursor()
    try:
        cur.execute("""
            SELECT
                UTILISATEURS.NOM AS NOM_UTILISATEUR,
                UTILISATEURS.PRENOM,
                LIVRES.TITRE,
                LIVRES.CHEMIN_LIVRE,  -- Ajout du chemin du livre
                DATE(EMPRUNTS.DATE_EMPRUNT) AS DATE_EMPRUNT,
                DATE(EMPRUNTS.DATE_ESTIMATION_RET) AS DATE_ESTIMATION_RET,
                EMPRUNTS.STATUT,
                EMPRUNTS.ID_EXEMPLAIRE
            FROM
                BIBLIOTECH.PUBLIC.EMPRUNTS
            JOIN
                BIBLIOTECH.PUBLIC.UTILISATEURS ON EMPRUNTS.ID_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
                BIBLIOTECH.PUBLIC.EXEMPLAIRES ON EMPRUNTS.ID_EXEMPLAIRE = EXEMPLAIRES.ID_EXEMPLAIRE
            JOIN
                BIBLIOTECH.PUBLIC.LIVRES ON EXEMPLAIRES.ID_LIVRE = LIVRES.ID_LIVRE
            WHERE
                EMPRUNTS.STATUT = 'En cours';
        """)

        results = cur.fetchall()
        emprunt_info = [{
            "NOM": result[0],
            "PRENOM": result[1],
            "TITRE": result[2],
            "CHEMIN_LIVRE": result[3],  # Chemin du livre
            "DATE_EMPRUNT": result[4],
            "DATE_ESTIMATION": result[5],
            "STATUT": result[6],
            "ID_EXEMPLAIRE": result[7]
        } for result in results]

        return emprunt_info
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()

def getall_Emprunt_count():
    conn = connection()
    cur = conn.cursor()
    try:
        # Requête pour compter le nombre de commandes de l'utilisateur
        cur.execute("""
            SELECT COUNT(*) FROM EMPRUNTS WHERE STATUT = 'En cours';
        """)
        count = cur.fetchone()[0]
        cur.close()
        conn.close()
        return count
    except Exception as e:
        return {'error': str(e)}, 500
    

def get_info_exemplaire(data):
    conn = connection()
    cur = conn.cursor()
    id_exemplaire=data.get('id_exemplaire')
    try:
        cur.execute("""
            SELECT
            U.NOM AS NOM_UTILISATEUR,
            U.PRENOM,
            L.TITRE AS NOM_LIVRE,
            U.ID_UTILISATEUR
            FROM
                BIBLIOTECH.PUBLIC.EMPRUNTS E
            JOIN
                BIBLIOTECH.PUBLIC.UTILISATEURS U ON E.ID_UTILISATEUR = U.ID_UTILISATEUR
            JOIN
                BIBLIOTECH.PUBLIC.EXEMPLAIRES EX ON E.ID_EXEMPLAIRE = EX.ID_EXEMPLAIRE
            JOIN
                BIBLIOTECH.PUBLIC.LIVRES L ON EX.ID_LIVRE = L.ID_LIVRE
            WHERE
            E.ID_EXEMPLAIRE = %s;

        """, (id_exemplaire,))

        result= cur.fetchone()
        emprunt_info = {
            "NOM": result[0],
            "PRENOM": result[1],
            "TITRE": result[2],
            "ID_UTILISATEUR": result[3]
            } 
        return emprunt_info
    except Exception as e:
        return {'error': str(e)}, 500
    finally:
        cur.close()
        conn.close()


def inserer_message_admin(data):
    conn = connection()
    cur = conn.cursor()
    
    try:
        id_emetteur_admin = data.get('id_emetteur_admin')
        id_destinataire_utilisateur = data.get('id_destinataire_utilisateur')
        contenu_message = data.get('contenu_message')
        titre_message = data.get('titre_message')
        date_envoi = datetime.now().date()

        # Exécution de la requête d'insertion
        cur.execute("""
            INSERT INTO MESSAGES (
                ID_EMETTEUR_ADMIN, 
                ID_DESTINATAIRE_UTILISATEUR, 
                CONTENU_MESSAGE, 
                TITRE_MESSAGE, 
                DATE_ENVOI
            )
            VALUES (%s, %s, %s, %s, %s)
        """, (
            id_emetteur_admin,
            id_destinataire_utilisateur,
            contenu_message,
            titre_message,
            date_envoi
        ))

        # Valider la transaction
        conn.commit()
        
        # Fermer la connexion
        cur.close()
        conn.close()

        return True
    except Exception as e:
        return False, {'error': str(e)}



def lire_messages_admin(data):
    conn = connection()
    cur = conn.cursor()
    id_admin=data.get('id_admin')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU     
            FROM
                MESSAGES
            JOIN
                UTILISATEURS ON MESSAGES.ID_EMETTEUR_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            WHERE
                MESSAGES.ID_DESTINATAIRE_ADMIN = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_admin,))

        results = cur.fetchall()
        messages_info = [{
            "ID_MESSAGE": result[0],
            "NOM_EMETTEUR": result[1],
            "PRENOM_EMETTEUR": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6]
        } for result in results]

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}


def lire_one_messages_admin(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                ADMIN.USERNAME AS USERNAME
            FROM
                MESSAGES
            JOIN
                UTILISATEURS ON MESSAGES.ID_EMETTEUR_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
                ADMIN ON MESSAGES.ID_Destinataire_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_MESSAGE = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_message,))

        result = cur.fetchone()
        messages_info = {
            "ID_MESSAGE": result[0],
            "NOM_EMETTEUR": result[1],
            "PRENOM_EMETTEUR": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6],
            "USERNAME":result[7]
        } 

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}


def marquer_message_comme_lu(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')
    try:
        # Exécution de la requête de mise à jour
        cur.execute("""
            UPDATE MESSAGES
            SET STATUT_LU = TRUE
            WHERE ID_MESSAGE = %s
        """, (id_message,))

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return True
    except Exception as e:
        return False, {'error': str(e)}


def lire_messages_envoyer_admin(data):
    conn = connection()
    cur = conn.cursor()
    id_admin=data.get('id_admin')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU     
            FROM
                MESSAGES
            JOIN
                UTILISATEURS ON MESSAGES.ID_DESTINATAIRE_UTILISATEUR  = UTILISATEURS.ID_UTILISATEUR
            WHERE
                MESSAGES.ID_EMETTEUR_ADMIN = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_admin,))

        results = cur.fetchall()
        messages_info = [{
            "ID_MESSAGE": result[0],
            "NOM_DESTINATAIRE": result[1],
            "PRENOM_DESTINATAIRE": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6]
        } for result in results]

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}



def lire_one_messages_envoye_admin(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            SELECT
                MESSAGES.ID_MESSAGE,
                UTILISATEURS.NOM AS NOM_EMETTEUR,
                UTILISATEURS.PRENOM AS PRENOM_EMETTEUR,
                MESSAGES.CONTENU_MESSAGE,
                MESSAGES.TITRE_MESSAGE,
                MESSAGES.DATE_ENVOI,
                MESSAGES.STATUT_LU,
                ADMIN.USERNAME AS USERNAME
            FROM
                MESSAGES
            JOIN
                UTILISATEURS ON MESSAGES.ID_DESTINATAIRE_UTILISATEUR = UTILISATEURS.ID_UTILISATEUR
            JOIN
                ADMIN ON MESSAGES.ID_EMETTEUR_ADMIN = ADMIN.ID_ADMIN
            WHERE
                MESSAGES.ID_MESSAGE = %s
            ORDER BY
                MESSAGES.DATE_ENVOI DESC
        """, (id_message,))

        result = cur.fetchone()
        messages_info = {
            "ID_MESSAGE": result[0],
            "NOM_EMETTEUR": result[1],
            "PRENOM_EMETTEUR": result[2],
            "CONTENU_MESSAGE": result[3],
            "TITRE_MESSAGE": result[4],
            "DATE_ENVOI": result[5],
            "STATUT_LU": result[6],
            "USERNAME":result[7]
        } 

        # Valider la transaction
        conn.commit()

        # Fermer la connexion
        cur.close()
        conn.close()

        return messages_info

    except Exception as e:
        return False, {'error': str(e)}

def Delete_Message(data):
    conn = connection()
    cur = conn.cursor()
    id_message=data.get('id_message')

    try:
        # Exécution de la requête pour récupérer les messages destinés à l'administrateur
        cur.execute("""
            DELETE FROM MESSAGES
            WHERE
                MESSAGES.ID_MESSAGE = %s
            
        """, (id_message,))
        # Valider la transaction
        conn.commit()
        # Fermer la connexion
        cur.close()
        conn.close()
        return True
    except Exception as e:
        return False, {'error': str(e)}


def get_admins():
    conn=connection()
    cur=conn.cursor()
    cur = conn.cursor()
    cur.execute(f"SELECT ID_ADMIN,USERNAME FROM ADMIN")
    results = cur.fetchall()
    admins = [{"ID_ADMIN": result[0], "USERNAME": result[1]} for result in results]
    return admins